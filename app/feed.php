<?php

	function get_feed($tag = 'scl', $next = false, $token = '207764.ba5c579.cc71dbd69998454598765f61769c16ef') {
		$data = [];
		$url = 'https://api.instagram.com/v1/tags/' . $tag . '/media/recent?access_token=' . $token;
		$content = json_decode(file_get_contents($url));

		if($next === true && $content->pagination->next_url) {
			$url = $content->pagination->next_url;
		}

		foreach($content->data as $item) {
			$photo = [
				'location' => $item->location,
				'created_time' => $item->created_time,
				'link' => $item->link,
				'likes' => $item->likes,
				'images' => $item->images,
				'caption' => $item->caption,
				'id' => $item->id,
				'user' => $item->user
			];

			array_push($data, $photo);
		}

		return $data;
	}

	$result = get_feed($_GET['tag'], false);

	header('Content-Type: application/json');
	echo json_encode($result);
