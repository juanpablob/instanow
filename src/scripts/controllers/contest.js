/*
 * @name ContestController
 */
/* global app */

app
	.controller('ContestController', ['$scope', '$element', '$http', '$timeout', 'config', 'helper', 'error_handler', function($scope, $element, $http, $timeout, config, helper, error_handler) {
		var $self = this,
			$storage = $.sessionStorage,
			$vars = {

			};

		/*!
		 * Calls methods through jQuery events
		 * d
		 * @author juanpablob
		 * @since 2015-01-28
		 * @return void
		 */
		$scope.index = function() {
			$('<img>').attr('src', 'dist/assets/images/winner-jjo.jpg');
			$('<img>').attr('src', 'dist/assets/images/winner-meet-and-greet.jpg');

			$('a[data-contest]').click(function() {
				console.log('contest');
				$self.winner($(this).attr('data-contest'));
			});
		};

		$self.winner = function(which) {
			var items = $storage.get('photos'),
				winner = items[Math.floor(Math.random() * items.length)];

			$('.name').html(winner.user.full_name);
			$('.avatar').attr('src', winner.user.profile_picture);

			$('.instance').hide();
			$('.' + which).show();
		};


		/*!
		 * Magic start (Initialization)
		 *
		 * @author juanpablob
		 * @since 2014-10-15
		 */
		$scope.index();
	}]);
