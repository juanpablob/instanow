/**
 * @name HelpersFactory
 */

app
	.factory('helper', function(config) {
		var helper = {};

		/*!
		 * Ajax Url. Concat passed params (array) and convert it to a compatible string to pass as CakePHP URL parameters. I.e: /controller/action/param1/param2/param3
		 *
		 * @author juanpablob
		 * @since 2014-10-17
		 * @return string
		 * @param array params
		 */
		helper.ajax_url = function(params) {
			 return config.app_base_path + params.join('/');
		};

		/*!
		 * Is Mobile. Look for a match in userAgent according to most popular mobile devices
		 *
		 * @author juanpablob
		 * @since 2014-11-05
		 * @return boolean
		 */
		 helper.is_mobile = function() {
		 	return (/iphone|ipod|android|ie|blackberry|fennec/).test(navigator.userAgent.toLowerCase());
		 };

		return helper;
	});
