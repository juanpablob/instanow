/**
 * @name ErrorHandlerFactory
 */

app
	.factory('error_handler', function() {
		var error_handler = {};

		/*!
		 * Error handler. Analyzes the response code of ajax requests and trigger UI actions to show errors. If everything is ok, returns true.
		 *
		 * @author juanpablob
		 * @since 2014-10-17
		 * @return boolean
		 * @param object data
		 */
		error_handler.response = function(data) {
			if (data.code != 200) {
				alert(data.message);
				// some beautiful ui to show error

				return false;
			}

			return true;
		};

		return error_handler;
	});
