/**
 * @name InitFactory
 */

app
	.factory('init', function() {
		var init = {};

		/*!
		 * Wide initialization of UI components behavior
		 *
		 * @author juanpablob
		 * @since 2014-10-17
		 * @return void
		 */
		init.index = function() {
			$(document).on('click', 'a[href="#"]', function(e) {
				e.preventDefault();
			});
		};

		return init;
	});
