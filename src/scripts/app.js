/**
 * @name instanow
 *
 * Main module of the application.
 */

var app = angular.module('instanow', [])
	.constant('config', {
		version: '0.0.1',

		app_domain: '',
		app_base_path: '/',

		static_domain: '',
		static_base_path: ''
	})
	.run(['$rootScope', 'init', function($rootScope, init) {
		init.index();
	}]);
