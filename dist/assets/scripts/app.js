/**
 * @name instanow
 *
 * Main module of the application.
 */

var app = angular.module('instanow', [])
	.constant('config', {
		version: '0.0.1',

		app_domain: '',
		app_base_path: '/',

		static_domain: '',
		static_base_path: ''
	})
	.run(['$rootScope', 'init', function($rootScope, init) {
		init.index();
	}]);

/*
 * @name ContestController
 */
/* global app */

app
	.controller('ContestController', ['$scope', '$element', '$http', '$timeout', 'config', 'helper', 'error_handler', function($scope, $element, $http, $timeout, config, helper, error_handler) {
		var $self = this,
			$storage = $.sessionStorage,
			$vars = {

			};

		/*!
		 * Calls methods through jQuery events
		 * d
		 * @author juanpablob
		 * @since 2015-01-28
		 * @return void
		 */
		$scope.index = function() {
			$('<img>').attr('src', 'dist/assets/images/winner-jjo.jpg');
			$('<img>').attr('src', 'dist/assets/images/winner-meet-and-greet.jpg');

			$('a[data-contest]').click(function() {
				console.log('contest');
				$self.winner($(this).attr('data-contest'));
			});
		};

		$self.winner = function(which) {
			var items = $storage.get('photos'),
				winner = items[Math.floor(Math.random() * items.length)];

			$('.name').html(winner.user.full_name);
			$('.avatar').attr('src', winner.user.profile_picture);

			$('.instance').hide();
			$('.' + which).show();
		};


		/*!
		 * Magic start (Initialization)
		 *
		 * @author juanpablob
		 * @since 2014-10-15
		 */
		$scope.index();
	}]);

/*
 * @name FeedController
 */
/* global app */

app
	.controller('FeedController', ['$scope', '$element', '$http', '$timeout', 'config', 'helper', 'error_handler', function($scope, $element, $http, $timeout, config, helper, error_handler) {
		var $self = this,
			$storage = $.sessionStorage,
			$vars = {
				first_known: '',
				ads: [
					'dist/assets/ads/1.jpg',
					'dist/assets/ads/2.jpg',
					'dist/assets/ads/3.jpg',
					'dist/assets/ads/4.jpg',
					'dist/assets/ads/5.jpg',
					'dist/assets/ads/6.jpg',
					'dist/assets/ads/7.jpg',
					'dist/assets/ads/8.jpg',
					'dist/assets/ads/9.jpg',
					'dist/assets/ads/10.jpg',
					'dist/assets/ads/11.jpg',
					'dist/assets/ads/12.jpg',
					'dist/assets/ads/13.jpg'
				]
			};

		/*!
		 * Calls methods through jQuery events
		 * d
		 * @author juanpablob
		 * @since 2015-01-26
		 * @return void
		 */
		$scope.index = function() {
			$self.get_feed();
			$self.resize();
			$self.preload_images($vars.ads);

			$(window).resize(function() {
				$self.resize();
			});

			$('a[data-contest]').click(function() {
				$('#wrap').hide();
			});

			$('.backtofeed').click(function() {
				$('.instance').hide();

				$('#wrap').show();
			});
		};

		$self.preload_images = function(images) {
			angular.forEach(images, function(item, index) {
				$('<img>').attr('src', item).load();
			});
		};

		$self.resize = function() {
			var wWidth = $(window).width(),
				wHeight = $(window).height(),
				mTop = (wHeight - 450) / 2,
				mLeft = (wWidth - 450) / 2;

			$('.fill, .ad').css({
				'width': wWidth,
				'height': wHeight
			});

			$('.frame').css({
				'top': mTop,
				'left': mLeft
			});
		};

		$self.check_index = function(index) {
			var check = $.grep($storage.get('photos'), function(item, key) {
				return item.id === index;
			});

			if(check.length > 0) { // If @index exists, return true
				return true;
			}

			return false;
		};

		$self.get_feed = function() {
			if(!$storage.get('photos') && !$storage.get('first_known')) {
				$storage.set('photos', []);
				$storage.set('first_known', '');
			}

			console.log('hay ' + $storage.get('photos').length + ' fotos en el storage');

			$http.get('app/feed.json?tag=' + $element.attr('data-tag') + '&r=' + Math.floor(Math.random() * 500000))
				.success(function(data, status, headers) {
					if(status !== 200) {
						console.log('error');
						console.log(headers);

						$scope.show_feed();

						return;
					}

					var photos = [];

					console.log(data);
					console.log('ahora');
					console.log(data[0].id);
					console.log($storage.get('first_known'));

					if(data[0].id === $storage.get('first_known')) {
						console.log('There\'s no new photos to get.');

						$scope.show_feed();

						return;
					}

					angular.forEach(data, function(item, key) {
						if($self.check_index(item.id) === true) {
							console.log('Record already exists in $storage.photos');
						}
						else {
							$self.preload_images([item.images.standard_resolution.url]); // preload

							photos.push(item);
						}
					});

					$storage.set('first_known', data[0].id);
					console.log('seteo de first known....' + data[0].id);

					$storage.set('photos', photos.concat($storage.get('photos')));
					console.log($storage.get('photos').length);

					$scope.show_feed();
				})
				.error(function(data, status, headers) {
					console.log('Error while getting photos from feed. Try again.');
				});
		};

		$scope.show_feed = function() {
			var times = 0;

			angular.forEach($storage.get('photos'), function(item, key) {
				$scope.loop = $timeout(function() {
					times++;

					if(times === 4) {
						times = 0;

						$('.ad').attr('src', $vars.ads[Math.floor(Math.random() * $vars.ads.length)]);

						$('.overlay').show();
					}
					else {
						$('.overlay').hide();

						$('.photo').attr('src', item.images.standard_resolution.url);
					}

					if(key == $storage.get('photos').length - 1) {
						$self.get_feed();
						console.log('vamos de nuevo');
					}
				}, key * 4000);
			});
		};



		/*!
		 * Magic start (Initialization)
		 *
		 * @author juanpablob
		 * @since 2014-10-15
		 */
		$scope.index();
	}]);

/**
 * @name ErrorHandlerFactory
 */

app
	.factory('error_handler', function() {
		var error_handler = {};

		/*!
		 * Error handler. Analyzes the response code of ajax requests and trigger UI actions to show errors. If everything is ok, returns true.
		 *
		 * @author juanpablob
		 * @since 2014-10-17
		 * @return boolean
		 * @param object data
		 */
		error_handler.response = function(data) {
			if (data.code != 200) {
				alert(data.message);
				// some beautiful ui to show error

				return false;
			}

			return true;
		};

		return error_handler;
	});

/**
 * @name HelpersFactory
 */

app
	.factory('helper', function(config) {
		var helper = {};

		/*!
		 * Ajax Url. Concat passed params (array) and convert it to a compatible string to pass as CakePHP URL parameters. I.e: /controller/action/param1/param2/param3
		 *
		 * @author juanpablob
		 * @since 2014-10-17
		 * @return string
		 * @param array params
		 */
		helper.ajax_url = function(params) {
			 return config.app_base_path + params.join('/');
		};

		/*!
		 * Is Mobile. Look for a match in userAgent according to most popular mobile devices
		 *
		 * @author juanpablob
		 * @since 2014-11-05
		 * @return boolean
		 */
		 helper.is_mobile = function() {
		 	return (/iphone|ipod|android|ie|blackberry|fennec/).test(navigator.userAgent.toLowerCase());
		 };

		return helper;
	});

/**
 * @name InitFactory
 */

app
	.factory('init', function() {
		var init = {};

		/*!
		 * Wide initialization of UI components behavior
		 *
		 * @author juanpablob
		 * @since 2014-10-17
		 * @return void
		 */
		init.index = function() {
			$(document).on('click', 'a[href="#"]', function(e) {
				e.preventDefault();
			});
		};

		return init;
	});

/**
 * @name ValidateFactory
 */

app
	.factory('validate', function() {
		var validate = {};

		return validate;
	});
