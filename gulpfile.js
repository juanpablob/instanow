/*
 * Gulpfile
 * @author juanpablob <m.juanpablob@gmail.com>
 * @since 2015-01-28
 */

var gulp = require('gulp'),
	sass = require('gulp-sass'),
	gutil = require('gulp-util'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	watch = require('gulp-watch'),
	jshint = require('gulp-jshint'),

	src = './src',
	dist = './dist',
	bower = './bower_components';

var vendor_scripts = [
	bower + '/angular/angular.js',
	bower + '/jquery/dist/'
];

gulp.task('js', function () {
	return gulp.src('./src/**/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(concat('app.js'))
		.pipe(gulp.dest('./dist/assets/scripts'));
});

gulp.task('css', function () {
	return gulp.src('./src/styles/app.scss')
		.pipe(sass())
		.pipe(gulp.dest('./dist/assets/styles'));
});

gulp.task('default', function() {
	gulp.run('js');

	gulp.watch('./src/**/*.js', function() {
		gulp.run('js');
	});

	gulp.watch('./src/**/*.scss', function() {
		gulp.run('css');
	});
});
